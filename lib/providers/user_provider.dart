import 'package:flutter/cupertino.dart';
import 'package:motivation_app/Model/firestore_service.dart';
import 'package:motivation_app/Model/user.dart';

class User_provider with ChangeNotifier{
  final firestoreService = FirestoreService();

  //Data
 String _user;
 String _userId;
 //var uuid = Uuid();

//getters
  String get user => _user;
  String get userId => _userId;
  Stream<List<User>> get usuarios => firestoreService.getUsers();

//Setters
 set changeName(String pUser){
  _user = pUser;
  notifyListeners();
 }

}

