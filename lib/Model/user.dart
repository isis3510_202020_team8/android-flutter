import 'package:flutter/cupertino.dart';

class User{

  final String userId;
  final String name;
  final String pic;
  final String description;

  User({@required this.userId, this.name, this.pic, this.description});

  factory  User.fromJson(Map<String, dynamic> json){
    return User(
      userId: json['userId'],
      name: json['name'],
      pic: json['pic'],
      description: json['description']
    );
  }

  Map<String, dynamic> toMap(){
    return {
      'userId': userId,
      'name': name,
      'pic': pic,
      'description': description
    };
  }
}