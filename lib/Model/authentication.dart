import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:provider/provider.dart';

class Authentication with ChangeNotifier{

  //static var token;

  Future<void> signUp(String email, String pass) async{
    const url =
        'https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=AIzaSyBUTzEbMGtJUDQAWpIy36im3P8KI3pMUOc';

    final response = await http.post(url, body: json.encode(
      {
       'email': email,
        'password': pass,
        'returnSecureToken': true
      }
    ));

    final responseData = json.decode(response.body);
    if(responseData['idToken']==null)
      throw Exception(responseData['error']['message']);
    //token = responseData['idToken'];
  }

  Future<void> logIn(String email, String pass) async{
    const url =
        'https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyBUTzEbMGtJUDQAWpIy36im3P8KI3pMUOc';

    final response = await http.post(url, body: json.encode(
        {
          'email': email,
          'password': pass,
          'returnSecureToken': true
        }
    ));

    final responseData = json.decode(response.body);
    if(responseData['idToken']==null)
      throw Exception(responseData['error']['message']);
    //token = responseData['idToken'];
  }


}