import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:motivation_app/Model/user.dart';

class FirestoreService{
  FirebaseFirestore _db = FirebaseFirestore.instance;

//Get entries
  Stream <List<User>> getUsers(){
    return _db
        .collection('users')
        .snapshots()
        .map((snapshot) => snapshot.docs
        .map((doc) => User.fromJson(doc.data()))
        .toList());
  }

  //Upsert
Future<void> setUser(User user){
    var options = SetOptions(merge: true);
    return _db
        .collection('users')
        .doc(user.userId)
        .set(user.toMap());
}
//Delete
Future<void> removeUser(String userId){
    return _db
        .collection('users')
        .doc(userId)
        .delete();
}

}
