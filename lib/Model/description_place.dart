import 'package:flutter/material.dart';

class DescriptionPlace extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    final description = Container(
        margin: new EdgeInsets.only(
          top: 20,
          left: 20,
          right: 20
        ),
      child: Text(
        'Lorem ipsum ajá',
        style: TextStyle(
          fontSize: 16,
          fontWeight: FontWeight.bold,
          color: Colors.grey
        ),
      ),
    );

    final star = Container(
      margin: EdgeInsets.only(
        top: 323,
        right:3
      ),
      child: Icon(
        Icons.star,
        color: Colors.amber,
      ),
    );

    final title = Row(
      children: [
        Container(
          margin: EdgeInsets.only(
            top: 320,
            left:20,
            right:20
          ),
          child: Text(
            'Titulo',
            style: TextStyle(
              fontSize: 30,
              fontWeight: FontWeight.w900
            ),
          ),
        ),
        Row(
          children: [
            star,
            star,
            star,
            star,
            star
          ],
        )
      ],//children
    );//Row

    return Column(
      children: [
        title
      ],
    );
  }
}
