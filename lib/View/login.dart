import 'package:flutter/material.dart';
import 'package:motivation_app/Model/authentication.dart';
import 'file:///D:/AndroidStudioProjects/android-flutter/lib/View/signup.dart';
import 'package:motivation_app/Model/cards.dart';
import 'package:provider/provider.dart';
import 'package:data_connection_checker/data_connection_checker.dart';

class LoginPage extends StatefulWidget {
  static const routeName = '/login';

  Future<bool> connected(){

  }

  Future<bool> result() async{
    bool result = await DataConnectionChecker().hasConnection;
    if (result == true) {
      print('YAY! Free cute dog pics!');
    } else {
      print('No internet :( Reason:');
      print(DataConnectionChecker().lastTryResults);
    }
  }

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  var email = '';
  var password = '';

  var alert = (dtitle, dbody, dtext) => showDialog<void>(
    barrierDismissible: false, // user must tap button!
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text(dtitle),
        content: SingleChildScrollView(
          child: ListBody(
            children: <Widget>[
              Text(dbody),
              Text(dtext),
            ],
          ),
        ),
        actions: <Widget>[
          FlatButton(
            child: Text('Approve'),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      );
    },
  );

  Future<void> _submit() async{
    if(email == '' || password==''||!email.contains("@") || !email.contains('.')||password.length < 8){
      return alert('Review fields', 'It can be a field missing or with failures', 'Pass must have more than 8 letters,'
          'and the email it has a format example@corp.com');
    }
    try{
      await Provider.of<Authentication>(context, listen: false).logIn(email, password);
      Navigator.of(context).pushReplacementNamed(Cards.routeName);
    }catch(err){
      return alert('Error', 'it happens an error while singin in', 'temp error message');
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        resizeToAvoidBottomPadding: false,
        body: Column(crossAxisAlignment: CrossAxisAlignment.start, children: <
            Widget>[
          Container( // ----------------------Title container----------------------
            child: Stack(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.fromLTRB(15.0, 110.0, 0.0, 0.0),
                  child: Text(
                    'LogIn',
                    style:
                    TextStyle(fontSize: 80.0, fontWeight: FontWeight.bold),
                  ),
                ),
              ],
            ),
          ),
          Container( // ----------------------General container----------------------
              padding: EdgeInsets.only(top: 35.0, left: 20.0, right: 20.0),
              child: Column(
                children: <Widget>[
                  TextField( // ---------------------- input email ----------------------
                    onChanged: (text){
                      setState(() {
                        email = text;
                      });
                    },
                    decoration: InputDecoration(
                        labelText: 'EMAIL',
                        labelStyle: TextStyle(
                            fontFamily: 'Montserrat',
                            fontWeight: FontWeight.bold,
                            color: Colors.grey),
                        focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.cyan))),
                  ),
                  SizedBox(height: 10.0),
                  TextField( // ---------------------- input password----------------------
                    onChanged: (text){
                      setState(() {
                        password = text;
                      });
                    },
                    decoration: InputDecoration(
                        labelText: 'PASSWORD ',
                        labelStyle: TextStyle(
                            fontFamily: 'Montserrat',
                            fontWeight: FontWeight.bold,
                            color: Colors.grey),
                        focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.cyan))),
                    obscureText: true, // password cant be seen
                  ),
                  SizedBox(height: 10.0),
                  SizedBox(height: 50.0),
                  Container(// ---------------------- sign up ----------------------
                      height: 40.0,
                      child: Material(
                        borderRadius: BorderRadius.circular(20.0),
                        shadowColor: Colors.blueAccent,
                        color: Colors.cyan,
                        elevation: 7.0,
                        child: InkWell(
                          onTap: () {
                            _submit();
                          },
                          child: Center(
                            child: Text(
                              'LOGIN',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                  fontFamily: 'Montserrat'),
                            ),
                          ),
                        ),
                      )),
                  SizedBox(height: 20.0),
                  Container(// ---------------------- go signup ----------------------
                    height: 40.0,
                    color: Colors.transparent,
                    child: Container(
                      decoration: BoxDecoration(
                          border: Border.all(
                              color: Colors.black,
                              style: BorderStyle.solid,
                              width: 1.0),
                          color: Colors.transparent,
                          borderRadius: BorderRadius.circular(20.0)),
                      child: InkWell(
                        onTap: (){
                          Navigator.of(context).pushReplacementNamed(SignupPage.routeName);
                         // Navigator.of(context).pop();
                        },
                        child: Center(
                          child: Text('SignUp',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontFamily: 'Montserrat')),
                        ),
                      ),
                    ),
                  ),
                ],
              )),
        ]));
  }
}